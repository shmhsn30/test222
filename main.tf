terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  access_key = "AKIAY6SXPPQUNJXY3PUH"
  secret_key = "y7Za1YM2cEew6q34KF//ilBWH/92BpUKs63YWWXH"
}

# Create a VPC
# resource "aws_vpc" "example" {
#   cidr_block = "10.0.0.0/16"
# }

# Create an S3 bucket
resource "aws_s3_bucket" "mybucket" {
  bucket = "sheham-s3-lambdaerssdadaswr1"
}

# Upload a text file to the S3 bucket
resource "aws_s3_bucket_object" "myobject" {
  bucket = aws_s3_bucket.mybucket.id
  key    = "xyz.txt"
  source = "xyz.txt"
}

# Create a Lambda function
resource "aws_lambda_function" "myfunction" {
  function_name = "my-function"
  handler       = "index.handler"
  runtime       = "python3.9"
  role          = aws_iam_role.myrole.arn
  timeout       = 10

  # Configure environment variables for the Lambda function
  environment {
    variables = {
      BUCKET_NAME = aws_s3_bucket.mybucket.id
      FILE_NAME   = aws_s3_bucket_object.myobject.key
    }
  }

  # Provide the Lambda function code
  filename      = "index.zip"
  source_code_hash = filebase64sha256("index.zip")
}

# IAM role for the Lambda function
resource "aws_iam_role" "myrole" {
  name = "my-role"

  # Attach the necessary policies for S3 and Lambda
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# IAM policy for the Lambda function's role to access S3
resource "aws_iam_policy" "mypolicy" {
  name   = "my-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::${aws_s3_bucket.mybucket.id}/*"
      ]
    }
  ]
}
EOF
}

# Attach the IAM policy to the Lambda function's role
resource "aws_iam_role_policy_attachment" "mypolicyattachment" {
  role       = aws_iam_role.myrole.name
  policy_arn = aws_iam_policy.mypolicy.arn
}